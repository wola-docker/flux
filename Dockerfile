FROM alpine:3.16.2

ARG FLUX_VERSION=0.32.0
RUN apk --no-cache upgrade && \
    apk --no-cache add \
    curl \
    git \
    bash && \
    curl -s https://fluxcd.io/install.sh | bash
